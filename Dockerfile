FROM centos:7.4.1708

RUN yum -y update openssl \
 && yum -y install epel-release \
 && yum -y install nodejs reposync createrepo wget \
 && yum --enablerepo=* clean all \
 && rm -rf /var/cache/yum \
 && rm -f /etc/yum.repos.d/*.repo \
 && adduser reposync

COPY . /app/
RUN chmod 750 /app/reposync.sh \
 && cd /app/ \
 && npm install


USER reposync
ENTRYPOINT ["/bin/node", "/app/app.js"]
EXPOSE 8080
